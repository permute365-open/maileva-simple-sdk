# OpenAPI\Client\EnvoiApi

All URIs are relative to https://api.sandbox.maileva.net/mail/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**sendingsGet()**](EnvoiApi.md#sendingsGet) | **GET** /sendings | Liste des envois |
| [**sendingsPost()**](EnvoiApi.md#sendingsPost) | **POST** /sendings | Création d&#39;un envoi |
| [**sendingsSendingIdDelete()**](EnvoiApi.md#sendingsSendingIdDelete) | **DELETE** /sendings/{sending_id} | Suppression d&#39;un envoi |
| [**sendingsSendingIdGet()**](EnvoiApi.md#sendingsSendingIdGet) | **GET** /sendings/{sending_id} | Détail d&#39;un envoi |
| [**sendingsSendingIdPatch()**](EnvoiApi.md#sendingsSendingIdPatch) | **PATCH** /sendings/{sending_id} | Modification partielle d&#39;un envoi |
| [**sendingsSendingIdSubmitPost()**](EnvoiApi.md#sendingsSendingIdSubmitPost) | **POST** /sendings/{sending_id}/submit | Finalisation d&#39;un envoi |


## `sendingsGet()`

```php
sendingsGet($start_index, $count): \OpenAPI\Client\Model\SendingsResponse
```

Liste des envois

Permet d'obtenir la liste des envois.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start_index = 1; // float | Le premier élément à retourner
$count = 50; // float | Le nombre d'élément à retourner

try {
    $result = $apiInstance->sendingsGet($start_index, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->sendingsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **start_index** | **float**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **float**| Le nombre d&#39;élément à retourner | [optional] [default to 50] |

### Return type

[**\OpenAPI\Client\Model\SendingsResponse**](../Model/SendingsResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsPost()`

```php
sendingsPost($sending_creation): \OpenAPI\Client\Model\SendingResponse
```

Création d'un envoi

Permet de créer un envoi. Cet envoi sera en état de brouillon (DRAFT), il faudra soummettre cet envoi pour qu'il soit envoyé en production.  Les principales options sont : - Le coloris d'impression : couleur ou noir et blanc, - Le format d'impression : recto-verso ou recto seul, - L'ajout d'une page porte-adresse, - Le type d'enveloppe est choisi automatiquement. 1 à 5 feuilles (feuille porte-adresse et enveloppe retour incluses) : enveloppe DL. 6 à 45 feuilles (hors feuille porte-adresse, enveloppe retour incluse) : enveloppe C4. - Le type de fenêtre sur l'enveloppe : simple ou double fenêtre - Le type d'affranchissement : rapide ou économique - e-mail de notification, - Impression de l'adresse expéditeur, - Durée d'archivage : 0 an, 1 an, 3 ans, 6 ans ou 10 ans - gestion électronique des plis non distribuables (PND) - ajout d'une enveloppe retour

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_creation = new \OpenAPI\Client\Model\SendingCreation(); // \OpenAPI\Client\Model\SendingCreation

try {
    $result = $apiInstance->sendingsPost($sending_creation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->sendingsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_creation** | [**\OpenAPI\Client\Model\SendingCreation**](../Model/SendingCreation.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\SendingResponse**](../Model/SendingResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdDelete()`

```php
sendingsSendingIdDelete($sending_id)
```

Suppression d'un envoi

Permet de supprimer un envoi.  Seuls les envois en état de brouillon (DRAFT) peuvent être supprimés.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $apiInstance->sendingsSendingIdDelete($sending_id);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->sendingsSendingIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdGet()`

```php
sendingsSendingIdGet($sending_id): \OpenAPI\Client\Model\SendingResponse
```

Détail d'un envoi

Permet de récupérer le détail d'un envoi à partir de son identifiant.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $result = $apiInstance->sendingsSendingIdGet($sending_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->sendingsSendingIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

[**\OpenAPI\Client\Model\SendingResponse**](../Model/SendingResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdPatch()`

```php
sendingsSendingIdPatch($sending_id, $sending_update): \OpenAPI\Client\Model\SendingResponse
```

Modification partielle d'un envoi

Permet de modifier un envoi.  Seuls les envois en état de brouillon (DRAFT) peuvent être modifiés.  Seuls les paramètres pour lesquels une valeur est fournie sont modifiés.  Si votre système ne permet pas d'utiliser le verbe PATCH, il est possible d'utiliser le verbe POST.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$sending_update = new \OpenAPI\Client\Model\SendingUpdate(); // \OpenAPI\Client\Model\SendingUpdate

try {
    $result = $apiInstance->sendingsSendingIdPatch($sending_id, $sending_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->sendingsSendingIdPatch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **sending_update** | [**\OpenAPI\Client\Model\SendingUpdate**](../Model/SendingUpdate.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\SendingResponse**](../Model/SendingResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/merge-patch+json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdSubmitPost()`

```php
sendingsSendingIdSubmitPost($sending_id)
```

Finalisation d'un envoi

Permet de soumettre l'envoi et de déclencher ainsi la demande de production.  Un envoi soumis ne peut pas être annulé.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $apiInstance->sendingsSendingIdSubmitPost($sending_id);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->sendingsSendingIdSubmitPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
