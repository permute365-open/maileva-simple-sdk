# OpenAPI\Client\DestinatairesApi

All URIs are relative to https://api.sandbox.maileva.net/mail/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**patchRecipient()**](DestinatairesApi.md#patchRecipient) | **PATCH** /sendings/{sending_id}/recipients/{recipient_id} | Modification partielle d&#39;un destinataire |
| [**sendingsSendingIdRecipientsDelete()**](DestinatairesApi.md#sendingsSendingIdRecipientsDelete) | **DELETE** /sendings/{sending_id}/recipients | Suppression de tous les destinataires |
| [**sendingsSendingIdRecipientsGet()**](DestinatairesApi.md#sendingsSendingIdRecipientsGet) | **GET** /sendings/{sending_id}/recipients | Liste des destinataires d&#39;un envoi |
| [**sendingsSendingIdRecipientsImportsPost()**](DestinatairesApi.md#sendingsSendingIdRecipientsImportsPost) | **POST** /sendings/{sending_id}/recipients/imports | Ajout d&#39;un ou de plusieurs destinataires à un envoi |
| [**sendingsSendingIdRecipientsPost()**](DestinatairesApi.md#sendingsSendingIdRecipientsPost) | **POST** /sendings/{sending_id}/recipients | Ajout d&#39;un destinataire à l&#39;envoi |
| [**sendingsSendingIdRecipientsRecipientIdDelete()**](DestinatairesApi.md#sendingsSendingIdRecipientsRecipientIdDelete) | **DELETE** /sendings/{sending_id}/recipients/{recipient_id} | Suprression d&#39;un destinataire |
| [**sendingsSendingIdRecipientsRecipientIdDeliveryStatusesGet()**](DestinatairesApi.md#sendingsSendingIdRecipientsRecipientIdDeliveryStatusesGet) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/delivery_statuses | Liste des statuts de distribution d&#39;un destinataire |
| [**sendingsSendingIdRecipientsRecipientIdDownloadArchiveGet()**](DestinatairesApi.md#sendingsSendingIdRecipientsRecipientIdDownloadArchiveGet) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_archive | Téléchargement du courrier envoyé au destinataire |
| [**sendingsSendingIdRecipientsRecipientIdGet()**](DestinatairesApi.md#sendingsSendingIdRecipientsRecipientIdGet) | **GET** /sendings/{sending_id}/recipients/{recipient_id} | Détail d&#39;un destinataire |


## `patchRecipient()`

```php
patchRecipient($sending_id, $recipient_id, $recipient_creation): \OpenAPI\Client\Model\RecipientResponse
```

Modification partielle d'un destinataire

Permet de modifier partiellement un destinataire

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire
$recipient_creation = new \OpenAPI\Client\Model\RecipientCreation(); // \OpenAPI\Client\Model\RecipientCreation

try {
    $result = $apiInstance->patchRecipient($sending_id, $recipient_id, $recipient_creation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->patchRecipient: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |
| **recipient_creation** | [**\OpenAPI\Client\Model\RecipientCreation**](../Model/RecipientCreation.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RecipientResponse**](../Model/RecipientResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsDelete()`

```php
sendingsSendingIdRecipientsDelete($sending_id)
```

Suppression de tous les destinataires

Permet de supprimer tous les destinataires d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $apiInstance->sendingsSendingIdRecipientsDelete($sending_id);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsGet()`

```php
sendingsSendingIdRecipientsGet($sending_id, $start_index, $count): \OpenAPI\Client\Model\RecipientsResponse
```

Liste des destinataires d'un envoi

Permet de récupérer la liste des destinataires d'un envoi. Cette liste peut être paginée. Par défaut, la pagination est de 50 résultats. Elle peut atteindre 500 au maximum.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$start_index = 1; // float | Le premier élément à retourner
$count = 50; // float | Le nombre d'élément à retourner

try {
    $result = $apiInstance->sendingsSendingIdRecipientsGet($sending_id, $start_index, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **start_index** | **float**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **float**| Le nombre d&#39;élément à retourner | [optional] [default to 50] |

### Return type

[**\OpenAPI\Client\Model\RecipientsResponse**](../Model/RecipientsResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsImportsPost()`

```php
sendingsSendingIdRecipientsImportsPost($sending_id, $import_recipients): \OpenAPI\Client\Model\RecipientsImportResponse
```

Ajout d'un ou de plusieurs destinataires à un envoi

Permet d'ajouter un ou plusieurs destinataires à un envoi. Le nombre de destinataires à l'importation est limité à 30 000.  Chaque ligne d’adresse doit contenir au plus 38 caractères. La ligne d’adresse 1 ou 2 doit être définie. La ligne d’adresse 6 doit être définie. Pour les adresses françaises, la ligne d’adresse 6 doit contenir  un code postal sur 5 chiffres, suivi d’un espace, suivi d’une ville.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$import_recipients = new \OpenAPI\Client\Model\ImportRecipients(); // \OpenAPI\Client\Model\ImportRecipients

try {
    $result = $apiInstance->sendingsSendingIdRecipientsImportsPost($sending_id, $import_recipients);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsImportsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **import_recipients** | [**\OpenAPI\Client\Model\ImportRecipients**](../Model/ImportRecipients.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\RecipientsImportResponse**](../Model/RecipientsImportResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsPost()`

```php
sendingsSendingIdRecipientsPost($sending_id, $recipient_creation): \OpenAPI\Client\Model\RecipientResponse
```

Ajout d'un destinataire à l'envoi

Permet d'ajouter un destinataire à l'envoi.  Le nombre de destinataires est limité à 30 000.  Chaque ligne d’adresse doit contenir au plus 38 caractères. La ligne d’adresse 1 ou 2 doit être définie. La ligne d’adresse 6 doit être définie.  Pour les adresses françaises, la ligne d’adresse 6 doit contenir un code  postal sur 5 chiffres, suivi d’un espace, suivi d’une ville.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_creation = new \OpenAPI\Client\Model\RecipientCreation(); // \OpenAPI\Client\Model\RecipientCreation

try {
    $result = $apiInstance->sendingsSendingIdRecipientsPost($sending_id, $recipient_creation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_creation** | [**\OpenAPI\Client\Model\RecipientCreation**](../Model/RecipientCreation.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\RecipientResponse**](../Model/RecipientResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsRecipientIdDelete()`

```php
sendingsSendingIdRecipientsRecipientIdDelete($sending_id, $recipient_id)
```

Suprression d'un destinataire

Permet de supprimer un destinataire d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $apiInstance->sendingsSendingIdRecipientsRecipientIdDelete($sending_id, $recipient_id);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsRecipientIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsRecipientIdDeliveryStatusesGet()`

```php
sendingsSendingIdRecipientsRecipientIdDeliveryStatusesGet($sending_id, $recipient_id, $start_index, $count): \OpenAPI\Client\Model\DeliveryStatusesResponse
```

Liste des statuts de distribution d'un destinataire

Permet de lister les statuts de distribution d'un destinataire.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire
$start_index = 1; // float | Le premier élément à retourner
$count = 50; // float | Le nombre d'élément à retourner

try {
    $result = $apiInstance->sendingsSendingIdRecipientsRecipientIdDeliveryStatusesGet($sending_id, $recipient_id, $start_index, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsRecipientIdDeliveryStatusesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |
| **start_index** | **float**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **float**| Le nombre d&#39;élément à retourner | [optional] [default to 50] |

### Return type

[**\OpenAPI\Client\Model\DeliveryStatusesResponse**](../Model/DeliveryStatusesResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsRecipientIdDownloadArchiveGet()`

```php
sendingsSendingIdRecipientsRecipientIdDownloadArchiveGet($sending_id, $recipient_id): \SplFileObject
```

Téléchargement du courrier envoyé au destinataire

Permet de télécharger au format PDF le courrier envoyé au destinataire dans le cas où l'option d'archivage a été choisie.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->sendingsSendingIdRecipientsRecipientIdDownloadArchiveGet($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsRecipientIdDownloadArchiveGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/zip`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `sendingsSendingIdRecipientsRecipientIdGet()`

```php
sendingsSendingIdRecipientsRecipientIdGet($sending_id, $recipient_id): \OpenAPI\Client\Model\RecipientResponse
```

Détail d'un destinataire

Permet d'obtenir le détail d'un destinataire d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->sendingsSendingIdRecipientsRecipientIdGet($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->sendingsSendingIdRecipientsRecipientIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

[**\OpenAPI\Client\Model\RecipientResponse**](../Model/RecipientResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
