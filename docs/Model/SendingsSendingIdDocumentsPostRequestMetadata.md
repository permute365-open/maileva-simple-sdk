# # SendingsSendingIdDocumentsPostRequestMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**priority** | **int** | Ordre d&#39;impression du document | [optional]
**name** | **string** | nom du document | [optional]
**shrink** | **bool** | redimensionnement du document | [optional] [default to true]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
