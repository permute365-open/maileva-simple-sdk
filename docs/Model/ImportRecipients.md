# # ImportRecipients

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**import_recipients** | [**\OpenAPI\Client\Model\RecipientCreationImport[]**](RecipientCreationImport.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
