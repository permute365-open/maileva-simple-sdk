# # DocumentResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifiant du document |
**priority** | **int** | Ordre d&#39;impression du document |
**name** | **string** | Nom du document |
**type** | **string** | Type du document |
**pages_count** | **int** | Nombre de pages (disponible à partir du statut ACCEPTED) | [optional]
**sheets_count** | **int** | Nombre de feuilles (disponible à partir du statut ACCEPTED) | [optional]
**size** | **int** | Poids du document en octets |
**converted_size** | **int** | Poids en octets du document converti | [optional]
**shrink** | **bool** | Redimensionnement du document | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
