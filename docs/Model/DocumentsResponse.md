# # DocumentsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documents** | [**\OpenAPI\Client\Model\DocumentResponse[]**](DocumentResponse.md) |  |
**paging** | [**\OpenAPI\Client\Model\PagingResponse**](PagingResponse.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
