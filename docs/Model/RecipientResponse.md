# # RecipientResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifiant du destinataire |
**custom_id** | **string** | Identifiant du destinataire fourni par le client | [optional]
**custom_data** | **string** | Information libre fournie par le client | [optional]
**address_line_1** | **string** | Ligne d&#39;adresse n°1 (Société) | [optional]
**address_line_2** | **string** | Ligne d&#39;adresse n°2 (Civilité, Prénom, Nom) | [optional]
**address_line_3** | **string** | Ligne d&#39;adresse n°3 (Résidence, Bâtiement ...) | [optional]
**address_line_4** | **string** | Ligne d&#39;adresse n°4 (N° et libellé de la voie) | [optional]
**address_line_5** | **string** | Ligne d&#39;adresse n°5 (Lieu dit, BP...) | [optional]
**address_line_6** | **string** | Ligne d&#39;adresse n°6 (Code postal et ville) |
**country_code** | [**\OpenAPI\Client\Model\CountryCode**](CountryCode.md) |  |
**documents_override** | [**\OpenAPI\Client\Model\DocumentsOverrideItem[]**](DocumentsOverrideItem.md) | Liste de bribes de documents. Si ce champ n&#39;est pas renseigné,  le destinataire recevra tous les documents associé à l&#39;envoi.  Si ce champ est renseigné, le destinataire recevra la liste de  bribes de documents indiquées (dans l&#39;ordre des éléments du tableau). | [optional]
**postage_class** | [**\OpenAPI\Client\Model\RecipientPostageClass**](RecipientPostageClass.md) |  | [optional]
**status** | [**\OpenAPI\Client\Model\RecipientStatus**](RecipientStatus.md) |  |
**status_detail** | **string** | Détail d&#39;un statut (cause du rejet) | [optional]
**last_delivery_status** | **string** | Dernier statut de distribution | [optional]
**last_delivery_status_date** | **string** | Date du dernier statut de distribution | [optional]
**postage_price** | **float** | Coût de l&#39;affranchissement en euros | [optional]
**undelivered_mail_date** | **\DateTime** | Date du PND | [optional]
**archive_date** | **\DateTime** | Date d&#39;archivage du pli | [optional]
**archive_url** | **string** | URL de l&#39;archive du pli | [optional]
**pages_count** | **int** | Nombre de pages. Ce nombre de pages inclut l&#39;éventuelle page porte-adresse (payante ou obligatoire) mais n&#39;inclut pas les pages blanches ajoutées au verso par Maileva. | [optional]
**billed_pages_count** | **int** | Nombre de pages facturées (disponible à partir du statut ACCEPTED). Ce nombre de pages inclut la page porte-adresse payante (DL) mais n&#39;inclut pas la page porte-adresse obligatoire (C4) ni les pages blanches ajoutées au verso par Maileva. | [optional]
**sheets_count** | **int** | Nombre de feuilles (disponible à partir du statut ACCEPTED). Ce nombre de feuilles inclut la page porte-adresse éventuelle (payante ou obligatoire). | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
