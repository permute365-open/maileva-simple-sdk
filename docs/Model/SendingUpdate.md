# # SendingUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Nom de l&#39;envoi | [optional]
**custom_id** | **string** | Identifiant de l&#39;envoi défini par le client | [optional]
**custom_data** | **string** | Information libre fournie par le client | [optional]
**color_printing** | **bool** | Impression couleur | [optional] [default to true]
**duplex_printing** | **bool** | Impression recto verso | [optional] [default to true]
**optional_address_sheet** | **bool** | Feuille porte adresse optionnelle | [optional] [default to false]
**notification_email** | **string** | E-mail du destinataire des notifications | [optional]
**print_sender_address** | **bool** | Impression de l&#39;adresse expéditeur | [optional] [default to false]
**sender_address_line_1** | **string** | Ligne d&#39;adresse n°1 (Société) de l&#39;expéditeur | [optional]
**sender_address_line_2** | **string** | Ligne d&#39;adresse n°2 (Civilité, Prénom, Nom) de l&#39;expéditeur | [optional]
**sender_address_line_3** | **string** | Ligne d&#39;adresse n°3 (Résidence, Bâtiement ...) de l&#39;expéditeur | [optional]
**sender_address_line_4** | **string** | Ligne d&#39;adresse n°4 (N° et libellé de la voie) de l&#39;expéditeur | [optional]
**sender_address_line_5** | **string** | Ligne d&#39;adresse n°5 (Lieu dit, BP...) de l&#39;expéditeur | [optional]
**sender_address_line_6** | **string** | Ligne d&#39;adresse n°6 (Code postal et ville) de l&#39;expéditeur | [optional]
**sender_country_code** | [**\OpenAPI\Client\Model\CountryCode**](CountryCode.md) |  | [optional]
**archiving_duration** | **int** | Durée d&#39;archivage en années | [optional] [default to self::ARCHIVING_DURATION_0]
**return_envelope** | [**\OpenAPI\Client\Model\ReturnEnvelope**](ReturnEnvelope.md) |  | [optional]
**envelope_windows_type** | **string** | enveloppe simple ou double fenêtre (si format DL) | [optional]
**postage_type** | [**\OpenAPI\Client\Model\PostageType**](PostageType.md) |  | [optional]
**treat_undelivered_mail** | **bool** | Gestion électronique des PND | [optional] [default to false]
**notification_treat_undelivered_mail** | **string[]** | Liste des emails de notification des PND | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
