# # DocumentsOverrideItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_id** | **string** | Identifiant du document | [optional]
**start_page** | **int** | Première page. Page 1 du document par défaut. | [optional]
**end_page** | **int** | Dernière page. Dernière page du document par défaut. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
