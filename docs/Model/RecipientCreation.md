# # RecipientCreation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_id** | **string** | Identifiant du destinataire fourni par le client | [optional]
**custom_data** | **string** | Information libre fournie par le client | [optional]
**address_line_1** | **string** | Ligne d&#39;adresse n°1 (Société) | [optional]
**address_line_2** | **string** | Ligne d&#39;adresse n°2 (Civilité, Prénom, Nom) | [optional]
**address_line_3** | **string** | Ligne d&#39;adresse n°3 (Résidence, Bâtiement ...) | [optional]
**address_line_4** | **string** | Ligne d&#39;adresse n°4 (N° et libellé de la voie) | [optional]
**address_line_5** | **string** | Ligne d&#39;adresse n°5 (Lieu dit, BP...) | [optional]
**address_line_6** | **string** | Ligne d&#39;adresse n°6 (Code postal et ville) |
**country_code** | [**\OpenAPI\Client\Model\CountryCode**](CountryCode.md) |  |
**documents_override** | [**\OpenAPI\Client\Model\DocumentsOverrideItem[]**](DocumentsOverrideItem.md) | Liste de bribes de documents. Si ce champ n&#39;est pas renseigné,  le destinataire recevra tous les documents associé à l&#39;envoi.  Si ce champ est renseigné, le destinataire recevra la liste de  bribes de documents indiquées (dans l&#39;ordre des éléments du tableau). | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
