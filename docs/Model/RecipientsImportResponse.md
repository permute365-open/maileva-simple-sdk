# # RecipientsImportResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accepted_import** | [**\OpenAPI\Client\Model\AcceptedImport[]**](AcceptedImport.md) |  |
**rejected_import** | [**\OpenAPI\Client\Model\RejectedImport[]**](RejectedImport.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
